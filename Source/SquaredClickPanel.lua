--[[
    Author: Ketsui, So End Our Foes
    Author: isodood, in its previous incarnation SquaredHealBot
    Licensed though GPL v3 (http://war.curseforge.com/licenses/7-gnu-general-public-license-version-3-gplv3/)
    Copyrighted under all statements of the GPL v3 license.
--]]
if not SquaredConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

-- Our panel window
local W

panel.title = L"Click!"

-- Create the panel
function CreatePanel(self)
    
    if W and W.name then return W end
    
    W = LibGUI("Blackframe")
    W:Resize(500, 650)
    
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"Squared Click")
    W.Title = e
    
    -- 'disable' label
    e = W("Label")
    e:Resize(270)
    e:Position(90,50)
    e:Align("leftcenter")
    e:SetText(L"Disable Squared Click:")
    W.LabelDisable = e
    
    -- 'disable' Checkbox
    e = W("Checkbox")
    e:AnchorTo(W.LabelDisable, "right", "left", 10, 0)
    e.OnLButtonUp =
        function()
            W.CheckClear:SetEnabled(not W.CheckDisable:GetValue())
        end
    W.CheckDisable = e
    
    -- 'cleartarget' label
    e = W("Label")
    e:Resize(220)
    e:AnchorTo(W.LabelDisable, "bottomleft", "topleft", 0, 10)
    e:Align("leftcenter")
    e:SetText(L"Clear Target:")
    W.LabelClear = e
    
    -- 'cleartarget' Checkbox
    e = W("Checkbox")
    e:AnchorTo(W.LabelClear, "right", "left", 60, 0)
    W.CheckClear = e
     	
    local possible = SquaredClickConfig.POSSIBLE_MODIFIERS
    local relative = nil
    local parent = e.parent
    for k, v in pairs(possible) do
        local window = SquaredClick.GetAbilityAssignmentWindow(v)
        d("Creating "..window)
        
        CreateWindowFromTemplate(window, "SquaredClickSettingsAssignmentTemplate", parent)
        if relative then
            WindowAddAnchor(window, "bottom", relative, "top", 0, 0)
        else
            WindowAddAnchor(window, "topleft", parent, "topleft", 60, 175)
        end
        
        LabelSetText(window.."_Mod", SquaredClickConfig.GetMessage(k))
        local texture, x, y = GetIconData(SquaredClickConfig.EMPTY_ICON)
        if texture then
            DynamicImageSetTexture(window.."_Icon", texture, x, y)
        end
        relative = window
    end

    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function ApplyPanel()
    
    local disable, orig_disable
    
    -- Apply 'disable'
    disable = W.CheckDisable:GetValue()
    orig_disable = SquaredClickConfig.GetSetting("Disabled")
    _ = orig_disable ~= disable and SquaredClickConfig.SetSetting("Disabled", disable)
        
    -- Apply 'cleartarget'
    disable = W.CheckClear:GetValue()
    orig_disable = SquaredClickConfig.GetSetting("ClearTarget")
    _ = orig_disable ~= disable and SquaredClickConfig.SetSetting("ClearTarget", disable)
    
    SquaredClickConfig.AcceptSettingsChanges()
    
    return
end

function UpdatePanel(self, key, value)
    	
    SquaredClickConfig.LoadSettings()
    	
    _ = (not key or key == "Disabled") and
        W.CheckDisable:SetValue(value or value == nil and SquaredClickConfig.GetSetting("Disabled"))
        
        W.CheckClear:SetEnabled(not W.CheckDisable:GetValue())
    
        _ = (not key or key == "ClearTarget") and
    W.CheckClear:SetValue(value or value == nil and SquaredClickConfig.GetSetting("ClearTarget"))
 
    return
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = SquaredConfigurator.AddPanel(panel)
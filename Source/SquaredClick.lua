--[[
    Author: Ketsui, So End Our Foes
    Author: isodood, in its previous incarnation SquaredHealBot
    Licensed though GPL v3 (http://war.curseforge.com/licenses/7-gnu-general-public-license-version-3-gplv3/)
    Copyrighted under all statements of the GPL v3 license.
--]]
 
if not SquaredClick then SquaredClick = {} end

--[[
    Initilization the addon
--]]
function SquaredClick.OnInitialize()
    SquaredClickConfig.InitLocale()
    SquaredClickConfig.InitializeSettings()
    SquaredClickConfig.PrintSettings()
	if not SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_DISABLED) then
        SquaredClick.Enable()
    end
    d("SquaredClick loaded into Squared.")
end

function SquaredClick.OnUpdate()

end

--[[
    Shutsdown the addon
--]]
function SquaredClick.OnShutdown()
	SquaredClick.Disable()
end

--[[
    Returns the window name for the given modifier
--]]
function SquaredClick.GetAbilityAssignmentWindow(modifier)
    return SquaredClickConfig.WINDOW_SETTINGS_ASSIGNMENTS..SquaredClickConfig.SEPARATOR..modifier
end

--[[
    Handles the drag-n-drop functionality for assigning abilities to the settings window
--]]
function SquaredClick.AbilityCursorSwap (flags, x, y)
    if (Cursor.IconOnCursor ()) then
        -- Only existing abilities that are not tactics can be dropped
        local abilityData = Player.GetAbilityData (Cursor.Data.ObjectId)
        if (abilityData == nil) then
            return
        end
        if (abilityData.abilityType == GameData.AbilityType.TACTIC) then
            return ActionButtonAlert (StringTables.Default.TEXT_TACTIC_DROP_ERROR)
        end
        -- assign cursor id as ability id
        local spellId = Cursor.Data.ObjectId
        Cursor.Clear ()
        -- get modifier
        local modifier = SystemData.MouseOverWindow.name:match(SquaredClickConfig.WINDOW_SETTINGS_ASSIGNMENTS..SquaredClickConfig.SEPARATOR.."([0-9]+)")
        if modifier then
            modifier = tonumber(modifier)
        end
        -- assign ability
        if modifier then
            local possible = SquaredClickConfig.POSSIBLE_MODIFIERS
            for k,v in pairs(possible) do
                if v == modifier then
                    SquaredClickConfig.AssignSettingsAbility(modifier, k, spellId)
                    return
                end
            end
        end
    end
end

--[[
    Disables the addon
--]]
function SquaredClick.Disable()
    -- return squared to previous state
    if Squared then
        -- unregister with squared
        Squared.UnregisterEventHandler("unitlclick", SquaredClick.UnitLClick)
    else
        SquaredClick.Print(SquaredClickConfig.GetMessage(SquaredClick.LOCAL_UNREGISTER_ERR))
    end
    -- unregister with event handlers
    UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST, "SquaredClick.OnPlayerEndCast")
    UnregisterEventHandler(SystemData.Events.SPELL_CAST_CANCEL, "SquaredClick.OnPlayerEndCast")
    UnregisterEventHandler(SystemData.Events.INTERACT_DONE, "SquaredClick.OnPlayerEndCast")
end


--[[
    Enables the addon
--]]
function SquaredClick.Enable()
    -- setup squared
    if Squared then
        -- register with squared
        Squared.RegisterEventHandler("unitlclick", SquaredClick.UnitLClick)
        -- show action buttons
        --SquaredClick.SetActionButtonVisibility(true)
    else
        SquaredClick.Print(SquaredClickConfig.GetMessage(SquaredClickConfig.LOCAL_REGISTER_ERR))
    end

    -- register with event handlers
    RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "SquaredClick.OnPlayerEndCast")
    RegisterEventHandler(SystemData.Events.SPELL_CAST_CANCEL, "SquaredClick.OnPlayerEndCast")
    RegisterEventHandler(SystemData.Events.INTERACT_DONE, "SquaredClick.OnPlayerEndCast")
end


--[[
    Sets the visibility of all the squared action buttons
--]]
function SquaredClick.SetActionButtonVisibility(isVisible)
    for g = 1, 10 do
        for m = 1, 6 do
            local window = "SquaredUnit_"..g.."_"..m.."Action"
            if DoesWindowExist(window) then
                WindowSetShowing(window, isVisible)
            end
        end
    end
end


--[[
    Verify Squared is installed, returns true if it is, otw false
--]]
function SquaredClick.VerifyState()
    return (Squared ~= nil)
end


--[[
    If the we are clearing the target after every cast, do it
--]]
function SquaredClick.OnPlayerEndCast(spellId, isChannel, castTime)
    -- if we are disabled, exit w/o doing anyting
    if SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_DISABLED) then
        return
    end
    -- if clear targeting is disabled, exit
    if not SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_CLEAR_TARGET) then
        return
    end
    -- if we are valid, continue
    if SquaredClick.VerifyState() then
        -- clear target
        d("Clearing target")
        ClearTarget(GameData.TargetType.FRIENDLY)
    end
end


--[[
    Targets the existing player
--]]
function SquaredClick.HandleTarget(target, spellId)
    -- is this a valid target - if not, reset
    TargetInfo:UpdateFromClient()
    local currentTarget = TargetInfo:UnitName("selffriendlytarget")
    -- if we have the same target, change action button
    if SquaredClick.ArePlayerNamesEqual(currentTarget, target) then
        if spellId and not IsTargetValid(spellId) then
            d(L"Target is not valid.")
            AlertTextWindow.AddLine(SystemData.AlertText.Types.COMBAT, SquaredClickConfig.GetMessage(SquaredClickConfig.LOCAL_INVALID_TARGET))
            return false
        end
        return true
    else
        -- we don't have the player, target api
        d(L"Target and player are not the same. Targeting "..target..L"...")
        if currentTarget then
            d(L"Current Target="..currentTarget)
        else
            d("Current Target=nil")
        end
        if target then
            d(L"Desired Target="..target)
        else
            d("Desired Target=nil")
        end
       	SquaredClick.TargetPlayer(target)
        return true
    end
end

--[[
    Target
--]]
function SquaredClick.TargetPlayer(target)
    SystemData.UserInput.ChatText = towstring(L"/target " .. target)
    BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SquaredClick.ArePlayerNamesEqual(p1, p2)
    if p1 and p2 then
        local p1N = p1:match(L"([^^]+)^?([^^]*)")
        local p2N = p2:match(L"([^^]+)^?([^^]*)")
        return p1N == p2N
    elseif not p1 and not p2 then
        return true
    end

    return false
end


--[[
    Set ability and target
--]]
function SquaredClick.UnitLClick(group, member, flags)
    -- if we are disabled, exit w/o doing anyting
    if SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_DISABLED) then
        return false
    end
    if SquaredClick.VerifyState() then
        local modifier = flags
        d("Modifier="..flags)
        local button = "SquaredUnit_"..group.."_"..member.."Action"
        local spellId = SquaredClick.GetSpellByModifier(modifier)
        local unit = Squared.GetUnit(group, member)
        local target = nil
        if unit then
            target = unit.name
        end
        if button and target then
            if spellId then
                -- handle target
                if SquaredClick.HandleTarget(target, spellId) then
                    -- assign correct ability
                    d("Assigning ability "..spellId.." to "..button)
                    SquaredClick.AssignAbility(spellId, button)
                else
                    -- clear the ability
                    d("Clear ability on "..button)
                    SquaredClick.SetAbilityToTarget(button, target)
                end
            else
                -- clear the ability
                d("Clear ability on "..button)
                SquaredClick.SetAbilityToTarget(button, target)
            end
        else
            local message = "Doing Nothing. "
            if not spellId then
                message = message.."Missing Spell Id. "
            end
            if not target then
                message = message.."Missing Target. "
            end
            if not button then
                message = message.."Missing Button."
            end
            d(message)
        end
    end
end


function SquaredClick.SetAbilityToTarget(window, target)
    SquaredClick.AssignAbility(0, window, target)
end

--[[
    Assigns the button the specified ability
    TODO: Fix this, here is a targetting and casting bug on oor and los
--]]
function SquaredClick.AssignAbility(actionId, window, target)
    local actionType = nil
    if actionId > 0 then
        actionType = GameData.PlayerActions.DO_ABILITY
        target = L""
    else
        actionType = GameData.PlayerActions.SET_TARGET
    end

    WindowSetGameActionTrigger (window, actionId)
    WindowSetGameActionData (window, actionType, actionId, target)
end

--[[
    Returns the ability id of the spell associated with the modifier combination
--]]
function SquaredClick.GetSpellByModifier(modifier)
    if modifier then
        d("Is Using Modifier - "..modifier)
    else
        d("Is Not Using Modifier.")
    end
    local spellId = nil
    local spells = SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_SPELLS)
    if spells then
        spellId = spells[modifier]
    end
    return spellId
end

--[[
    Sets player defaults
--]]
function SquaredClick.SetPlayerDefaults()
    SquaredClickSettings[WStringToString(GameData.Player.name)] = {}
    SquaredClickConfig.VerifyAllSettingsExist()
end

--[[
    Prints messages to console
--]]
function SquaredClick.Print(msg)
    EA_ChatWindow.Print(towstring(msg))
end

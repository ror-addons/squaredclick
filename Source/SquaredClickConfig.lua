--[[
    Licensed though GPL v3 (http://war.curseforge.com/licenses/7-gnu-general-public-license-version-3-gplv3/)
    Copyrighted under all statements of the GPL v3 license.
--]]

if not SquaredClickConfig then SquaredClickConfig = {} end

SquaredClickConfig.DEBUG = false

--[===[@debug@

SquaredClickConfig.DEBUG = true

--@end-debug@]===]

--settings constants
SquaredClickConfig.SETTINGS_SPELLS = "Spells"
SquaredClickConfig.SETTINGS_DISABLED = "Disabled"
SquaredClickConfig.SETTINGS_CLEAR_TARGET = "ClearTarget"

SquaredClickConfig.MODIFIERS_ALT = "ALT"
SquaredClickConfig.MODIFIERS_SHIFT = "SHIFT"
SquaredClickConfig.MODIFIERS_CTRL = "CTRL"
SquaredClickConfig.MODIFIERS_NONE = "NONE"

SquaredClickConfig.SETTINGS_DEFAULTS = {
    [SquaredClickConfig.SETTINGS_SPELLS] = {},
    [SquaredClickConfig.SETTINGS_CLEAR_TARGET] = false,
    [SquaredClickConfig.SETTINGS_DISABLED] = false,
}

SquaredClickConfig.MODIFIERS = {
    [SquaredClickConfig.MODIFIERS_ALT] = tonumber(SystemData.ButtonFlags.ALT),
    [SquaredClickConfig.MODIFIERS_SHIFT] = tonumber(SystemData.ButtonFlags.SHIFT),
    [SquaredClickConfig.MODIFIERS_CTRL] = tonumber(SystemData.ButtonFlags.CONTROL),
    [SquaredClickConfig.MODIFIERS_NONE] = 0,
}

SquaredClickConfig.POSSIBLE_MODIFIERS = {
    [SquaredClickConfig.MODIFIERS_ALT] = tonumber(SystemData.ButtonFlags.ALT),
    [SquaredClickConfig.MODIFIERS_SHIFT] = tonumber(SystemData.ButtonFlags.SHIFT),
    [SquaredClickConfig.MODIFIERS_CTRL] = tonumber(SystemData.ButtonFlags.CONTROL),
    [SquaredClickConfig.MODIFIERS_NONE] = 0,
    [SquaredClickConfig.MODIFIERS_ALT.."+"..SquaredClickConfig.MODIFIERS_SHIFT] = (tonumber(SystemData.ButtonFlags.ALT)+tonumber(SystemData.ButtonFlags.SHIFT)),
    [SquaredClickConfig.MODIFIERS_ALT.."+"..SquaredClickConfig.MODIFIERS_CTRL] = (tonumber(SystemData.ButtonFlags.ALT)+tonumber(SystemData.ButtonFlags.CONTROL)),
    [SquaredClickConfig.MODIFIERS_SHIFT.."+"..SquaredClickConfig.MODIFIERS_CTRL] = (tonumber(SystemData.ButtonFlags.SHIFT)+tonumber(SystemData.ButtonFlags.CONTROL)),
    [SquaredClickConfig.MODIFIERS_ALT.."+"..SquaredClickConfig.MODIFIERS_CTRL.."+"..SquaredClickConfig.MODIFIERS_SHIFT] = (tonumber(SystemData.ButtonFlags.ALT)+tonumber(SystemData.ButtonFlags.CONTROL)+tonumber(SystemData.ButtonFlags.SHIFT)),
}

--Constants
SquaredClickConfig.BUILD = "0001"
SquaredClickConfig.WINDOW = "SquaredClick"
SquaredClickConfig.WINDOW_SETTINGS = "SquaredClickPanel"
SquaredClickConfig.WINDOW_SETTINGS_ASSIGNMENTS = "SquaredClickConfigAssignmentWindow"
SquaredClickConfig.SEPARATOR = "_"
SquaredClickConfig.EMPTY_ICON = 1

--Localized Constants
SquaredClickConfig.LOCAL_SETTINGS_TITLE = "SETTINGS_TITLE"
SquaredClickConfig.LOCAL_INIT_DEFAULTS = "INIT_DEFAULTS"
SquaredClickConfig.LOCAL_INIT_ADDON = "INIT_ADDON"
SquaredClickConfig.LOCAL_UNREGISTER_ERR = "UNREG_ERR"
SquaredClickConfig.LOCAL_REGISTER_ERR = "REG_ERR"
SquaredClickConfig.LOCAL_INVALID_TARGET = "INVALID_TARGET"

SquaredClickConfig.MESSAGES = {}

-- normal vars
SquaredClickConfig.actions = {}
SquaredClickConfig.Messages = {}

--[[
    Loads localization settings
--]]
function SquaredClickConfig.InitLocale()
	local switch = {
		[SystemData.Settings.Language.ENGLISH] = L"enUS",
		[SystemData.Settings.Language.FRENCH] = L"frFR",
		[SystemData.Settings.Language.GERMAN] = L"deDE",
		[SystemData.Settings.Language.ITALIAN] = L"itIT",
		[SystemData.Settings.Language.SPANISH] = L"esES",
		[SystemData.Settings.Language.KOREAN] = L"koKR",
		[SystemData.Settings.Language.S_CHINESE] = L"zhCN",
		[SystemData.Settings.Language.T_CHINESE] = L"zhTW",
		[SystemData.Settings.Language.JAPANESE] = L"jaJP",
		[SystemData.Settings.Language.RUSSIAN] = L"ruRU",
	}
		
	local res = switch[SystemData.Settings.Language.active] 
	if not res then
		res = L"enUS"
	end

    SquaredClickConfig.Messages = SquaredClickConfig.MESSAGES[res]
end

--[[
    Loads settings
--]]
function SquaredClickConfig.InitializeSettings()
    -- create settings if we have none
    SquaredClickConfig.CreateSettings()

    -- load player settings
    if not SquaredClickSettings[WStringToString(GameData.Player.name)] then
        -- we have none - create the player settings
        d(SquaredClickConfig.GetMessage(SquaredClickConfig.LOCAL_INIT_DEFAULTS) .. GameData.Player.name .. L"...")
        SquaredClick.SetPlayerDefaults()
    else
        -- we have player settings - print out current settings
        SquaredClickConfig.VerifyAllSettingsExist()
        d(SquaredClickConfig.GetMessage(SquaredClickConfig.LOCAL_INIT_ADDON)..L"...")
    end
end

--[[
    Verifies that all the settings exist, if they do not, it will add the missing ones
    assigning them the default values
--]]
function SquaredClickConfig.VerifyAllSettingsExist()
    for k,v in pairs(SquaredClickConfig.SETTINGS_DEFAULTS) do
        d("Verifying setting "..k)
        local foundSetting = false
        
        -- search player settings for setting
        for k1,v1 in pairs(SquaredClickSettings[WStringToString(GameData.Player.name)]) do
            if k1 == k then
                foundSetting = true
            end
        end
        -- if we didnt find it - add the default
        if not foundSetting then
            local value = v
            if type(value)=="boolean" then
                if value then
                    value = "true"
                else
                    value = "false"
                end
            end
            --d("Creating setting key="..k..", value="..value)

            SquaredClickConfig.SetSetting(k, v)
        else
            local value = SquaredClickConfig.GetSetting(k)
            if type(value)=="boolean" then
                if value then
                    value = "true"
                else
                    value = "false"
                end
            end
            if type(value)~="table" then
                d("Found setting key="..k..", value="..value)
            else
                d("Found setting key="..k..", value=table")
            end

        end
    end
end



--[[
    Updates the settings window the specified ability/monitor assignment
--]]
function SquaredClickConfig.AssignSettingsAbility(modifierValue, modifier, actionId)
    local iconId = 0
    local name = L""
    if actionId or actionId == 0 then
        local abilityData = GetAbilityData(actionId)
        iconId = abilityData.iconNum
        name = abilityData.name
        SquaredClickConfig.actions[modifierValue] = actionId
    end
    local window = SquaredClick.GetAbilityAssignmentWindow(modifierValue)
    local iconWindow = window.."_Icon"
    local labelWindow = window.."_Text"
    if iconId == 0 then
    	iconId = SquaredClickConfig.EMPTY_ICON
    end 
    local texture, x, y = GetIconData(iconId)
    DynamicImageSetTexture(iconWindow, texture, x, y)
    LabelSetText(labelWindow, name)
end

--[[
    Clears the given ability in the settings window
--]]
function SquaredClickConfig.AbilityClear()
    -- get modifier
    local modifier = SystemData.MouseOverWindow.name:match(SquaredClickConfig.WINDOW_SETTINGS_ASSIGNMENTS..SquaredClickConfig.SEPARATOR.."([0-9]+)")
    if modifier then
        modifier = tonumber(modifier)
    end
    -- assign ability
    if modifier then
        local possible = SquaredClickConfig.POSSIBLE_MODIFIERS
        for k,v in pairs(possible) do
            if v == modifier then
                SquaredClickConfig.AssignSettingsAbility(modifier, k, 0)
                return
            end
        end
    end
end

--[[
    Applies the changes in the settings window
--]]
function SquaredClickConfig.AcceptSettingsChanges()
    SquaredClickConfig.SaveSettings()
end

--[[
    Cancels the changes in the settings window
--]]
function SquaredClickConfig.CancelSettingsChanges()
    SquaredClickConfig.SetSettingsVisible(false)
end

--[[
    Saves the settings into the window
--]]
function SquaredClickConfig.SaveSettings()
    local spells = SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_SPELLS)
    -- clear spells
    for k, v in pairs(spells) do
        table.remove(spells)
    end
    -- reassign spells
    for m, s in pairs(SquaredClickConfig.actions) do
        -- if we have a valid assignment, save
        if s >= 0  then
            spells[m] = s
        end
    end
end

--[[
    Loads the settings into the window
--]]
function SquaredClickConfig.LoadSettings()
    local assigned = SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_SPELLS)
    local possible = SquaredClickConfig.POSSIBLE_MODIFIERS
    -- loop thru possibles, determining if we have any values for them
    for k, v in pairs(possible) do
        -- do we have an assignment
        local spellId = assigned[v]
        local modifier = k
        local modifierValue = v
        -- populate record in table
        SquaredClickConfig.AssignSettingsAbility(modifierValue, modifier, spellId)
    end
end

--[[
    Sets specified option to the specified value (cannot be nil)
--]]
function SquaredClickConfig.SetSetting(key, value)
    local playerSettings = SquaredClickSettings[WStringToString(GameData.Player.name)]
    playerSettings[key] = value
end

--[[
    gets the value for the specified option, return nil if missing
--]]
function SquaredClickConfig.GetSetting(key)
    local playerSettings = SquaredClickSettings[WStringToString(GameData.Player.name)]
    return playerSettings[key]
end

--[[
    Initializes all settings for the first time
--]]
function SquaredClickConfig.CreateSettings()
    if not SquaredClickSettings then SquaredClickSettings = {} end
end

function SquaredClickConfig.GetMessage(key)
    return SquaredClickConfig.Messages[key]
end

--[[
    Prints out the current settings
--]]
function SquaredClickConfig.PrintSettings()
    -- we have player settings - print out current settings
    local optDisabled = "false"
    if SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_DISABLED) then
	    optDisabled = "true"
	end
	
    local optClearTargeting = "false"
    if SquaredClickConfig.GetSetting(SquaredClickConfig.SETTINGS_CLEAR_TARGET) then
		optClearTargeting = "true"
	end
	
    SquaredClick.Print("SquaredClick Options:")
    SquaredClick.Print("Build      : " .. SquaredClickConfig.BUILD)
    SquaredClick.Print("Disabled   : " .. optDisabled)
    SquaredClick.Print("ClearTarget: " .. optClearTargeting)
end

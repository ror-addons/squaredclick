<?xml version="1.0" encoding="UTF-8"?>
<!--
	Author: Ketsui, So End Our Foes
	Author: isodood, in its previous incarnation SquaredHealBot
    Licensed though GPL v3 (http://war.curseforge.com/licenses/7-gnu-general-public-license-version-3-gplv3/)
    Copyrighted under all statements of the GPL v3 license.
-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="SquaredClick" version="1.1" date="12/01/2010" >
	    <VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="saithra" email="klaus.myrseth@mac.com" />
		<Description text="Allows you to assign a key modifier (alt|shift|ctrl) to designate which spell should be cast on your unit frame target when you hold down that modifier and left-click with your mouse" />
        
        <Dependencies>
            <Dependency name="EASystem_TargetInfo"/>
            <Dependency name="EASystem_Utils"/>  
            <Dependency name="EA_ChatWindow"/>
            <Dependency name="Squared" />
            <Dependency name="SquaredConfigurator"/>
        </Dependencies>
        
		<Files>
			<File name="Source/SquaredClickConfig.lua" />
			<File name="Source/SquaredClick.lua" />
			<File name="Source/SquaredClick.xml" />
			<File name="Locales/enUS.lua" />
			<File name="Locales/deDE.lua" /> 
			<File name="Locales/esES.lua" /> 
			<File name="Locales/frFR.lua" /> 
			<File name="Locales/itIT.lua" /> 
			<File name="Locales/jaJP.lua" /> 
			<File name="Locales/koKR.lua" /> 
			<File name="Locales/ruRU.lua" /> 
			<File name="Locales/zhCN.lua" /> 
			<File name="Locales/zhTW.lua" />
			<!-- Panels -->
            <File name="Source/SquaredClickPanel.lua" />
		</Files>
		
		<SavedVariables>
            <SavedVariable name="SquaredClickSettings" />
        </SavedVariables>
		
		<OnInitialize>
      		<CallFunction name="SquaredClick.OnInitialize" />
		</OnInitialize>
		
		<OnUpdate/>
		
		<OnShutdown>
			SquaredClick.OnShutdown()
		</OnShutdown>
		
		<WARInfo>
		    <Categories>
		        <Category name="BUFFS_DEBUFFS" />
		        <Category name="RVR" />
		        <Category name="GROUPING" />
		    </Categories>
		    <Careers>
		        <Career name="DISCIPLE" />
		        <Career name="RUNE_PRIEST" />
		        <Career name="SHAMAN" />
		        <Career name="WARRIOR_PRIEST" />
		        <Career name="ZEALOT" />
		        <Career name="ARCHMAGE" />
		    </Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>